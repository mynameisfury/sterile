{
    "id": "82e2c7a4-2af8-4ee3-8ff1-155c8b0b3945",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c61981a-6a41-41fc-aaf0-43f099343792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82e2c7a4-2af8-4ee3-8ff1-155c8b0b3945",
            "compositeImage": {
                "id": "7757b980-2c10-41ed-919c-108eb31c7e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c61981a-6a41-41fc-aaf0-43f099343792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7810cd-35e5-4090-a31f-1f104a4c4ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c61981a-6a41-41fc-aaf0-43f099343792",
                    "LayerId": "c51a3be8-8e91-4e3e-8c93-36be77ee4b89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c51a3be8-8e91-4e3e-8c93-36be77ee4b89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82e2c7a4-2af8-4ee3-8ff1-155c8b0b3945",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}