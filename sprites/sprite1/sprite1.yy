{
    "id": "e82fc816-7f8f-4765-bcd3-52e0cee9e9a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d0dd47d-b899-4fef-8092-50bb837d416d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e82fc816-7f8f-4765-bcd3-52e0cee9e9a4",
            "compositeImage": {
                "id": "e08a9dc7-b19e-4804-919f-1cb0ae76f880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d0dd47d-b899-4fef-8092-50bb837d416d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a294c5-3598-420d-8b11-f366fa11be8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d0dd47d-b899-4fef-8092-50bb837d416d",
                    "LayerId": "f870950c-3c92-48b4-8db4-5720c1c3c25f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f870950c-3c92-48b4-8db4-5720c1c3c25f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e82fc816-7f8f-4765-bcd3-52e0cee9e9a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}